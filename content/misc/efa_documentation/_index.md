+++
template = "efa.html"
+++

# On Distance

On Distance was my artwork for the course "Electronics for Artists". In it I explore the multiple meanings of the word "distance" through poetry. The primary inspiration behind this work was being introduced to distance sensors, which I somehow related to, as I've struggled with depression and how it causes my perceived emotional distance to other people to grow. I guess in other words you could say that my own distance sensor has been malfunctioning recently.

The way the piece works is by gauging the distance to the viewer with a distance sensor, and displaying different, randomly selected, words that correspond to that distance. If the viewer is far away, words for emotional or physical distance are chosen, and similarly, if the viewer is close, words for intimacy and physical contact are used. 

With movement the viewer can communicate their ideas of distance to another person or a place by playing out physically the timeline of their feelings regarding that distance. The end result is a poem that corresponds to the users movements, which gets displayed for a while, and then is lost eternally, fleeting, like our feelings of distance. 

At first I was a bit skeptical how well I would be able to construct poems programmatically, but luckily due to poetry being quite freeform, it ended up being easier than I thought. 

The solution I ended up with has two groups of words: one for being far-away, and the other for being close. There are also various filler words, such as "I" or "while", different punctuation marks and whitespace. The distance the user is from the sensor acts as the probability for the program to choose between the two groups. Then, by completely random chance, filler words and whitespace/punctuation are inserted. It's very much based on RNG, but also works surprisingly well.

I also wanted to incorporate a CRT TV into the work, as I want to learn more about them and how to interface with them via integrated circuits. For this reason, the Teensy 4 was chosen for this piece, as it has a pretty robust [library](https://github.com/Jean-MarcHarvengt/VGA_t4) for interfacting with analog monitors via VGA. Splitting open a VGA cable and learning what all the leads inside do was super interesting to me, as funny as it sounds. 

This was not without it's problems though. At first I tried to use a normal TV via a VGA-Composite adapter with the Teensy, but it failed to work. It was only after I managed to loan an actual old PC monitor with a VGA out that I managed to get the library working. I guess working with a PAL/NTSC TV would require some additional work, and I'm eager study further how to output a PAL signal from a Teensy or a similar board.

Overall I'm very satisfied with the work, especially since I had my doubts about the interaction and the quality of the poems, but the result turned out better than I could have expected. Below are a photo and a video of the work for you to enjoy. Peace!

![A picture of the work, showing a poem on a CRT display](/img/on_distance.jpg)

![Video of the work, where the author paces back and forth in front of the CRT display, showing the differences in the words generated as a result.](/img/on_distance.MOV)