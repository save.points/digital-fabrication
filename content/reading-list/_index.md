+++
title = "Reading List"
template = "index.html"
+++

![Anime girl flipping through a book](/img/anime-book.gif)

# Reading List

A collection of interesting essays/videos/articles/whatever sorted by topic.

The checkmark next to a link is mainly for myself, as it simply means that I've read/watched said link.

## On Making

- [How Sustainable is Digital Fabrication?](https://solar.lowtechmagazine.com/2014/03/how-sustainable-is-digital-fabrication.html) by Low-Tech Magazine ✅
- [Critical Making](http://www.conceptlab.com/criticalmaking/)

## On Hacking

- [Hacking Defaults](http://users.dma.ucla.edu/~reas/HackingDefaults%20copy/Untitled%20Page.html) ✅

## On Software Development

- [Critical Engineering Manifesto](https://criticalengineering.org) ✅
- [Always Already Programming](https://gist.github.com/melaniehoff/95ca90df7ca47761dc3d3d58fead22d4) by Melanie Hoff ✅
- [Against software development](http://www.rntz.net/post/against-software-development.html) ✅

## On Social Media

- [Mirror of Your Mind](https://reallifemag.com/mirror-of-your-mind/) by Isabel Munson ✅

## On Technology

- [How To Kill Your Tech Industry](https://logicmag.io/failure/how-to-kill-your-tech-industry/) ✅

## On Video Games

- [ThorHighHeels](https://www.youtube.com/c/thorhighheels/videos)
- [ACTION BUTTON REVIEWS Tokimeki Memorial](https://www.youtube.com/watch?v=xb-DtICmPTY)
- [Cyberpunk 2077: some cyber, no punk | Sophie From Mars](https://www.youtube.com/watch?v=h_SzMP-V4mA) ✅
- [Playing Metal Gear Solid V: The Phantom Pain](https://www.newyorker.com/magazine/2020/01/06/playing-metal-gear-solid-v-the-phantom-pain) By Jamil Jan Kochai
- [Idolm@ster and the Mechanics of Depression](https://killscreen.com/previously/articles/idolmster-and-the-mechanics-of-depression/) by Caty McCarthy ✅
- [Infinity Blade Review](https://geekabouttown.com/oddities/infinity-blade/) by J. Nicholas Geist ✅

## On Anime/Manga

- [The Bizarre Rabbit Hole of Direct To VHS Anime](https://www.youtube.com/watch?v=mF-XaCPqjqE) ✅
- [The Curse of Evangelion](https://www.youtube.com/watch?v=rHIvs0Q-uKI) ✅
- [A Series of Moments - An Exploration of Slice of Life](https://www.youtube.com/watch?v=V9dOavON_p4) ✅