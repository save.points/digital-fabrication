+++
title = "Samuli Kärki"
+++

![A selfie showing myself with cool sunglasses](/img/selfie.gif)

Hey, I'm Samuli and you've stumbled into my digital domain. I'm a programmer and a digital artist currently studying in the New Media Design and Production Master's Programme at Aalto University in Otaniemi, Espoo.

My interests include, but are not limited to, CRT TV's, old storage media, forgotten video games and anime, the weird web, and metahumanism. In general all stuff that share a vibe with _Serial Experiments Lain_ and _Ghost in the Shell_.

In addition to being my homepage, this website acts as a learning diary for the Digital Fabrication -course held at Aalto. On said course I'm looking forwards to learning more about the hardware/electronics side of things, since I have a pretty good grasp on the programming side of things, or so I atleast would like to believe, being from a computer science background and what not.

With that I wish you a warm welcome and I hope you enjoy your stay!
