+++
title = "iPod Diary"
template = "index.html"
+++

This spring (2022) I decided to buy an iPod mini for cheap on the Finnish Craigslist equivalent, [Tori](https://tori.fi). The reason I bought it was both to become more active looking for music and detaching myself from my phone. This is a diary I keep of small observations I make while living life with it.

**22.4.** I went to the store to buy snacks. Forgot that I didn't have money and I couldn't transfer any without my phone. Had to walk back to transfer some money with my phone. At the store there were a few funny things to take photos of, but since I don't have a phone, I couldn't. Need to start carrying a point-and-shoot around.